# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import os 
from azure.storage.blob import BlobServiceClient

def main(fileNames: list) -> list:
    try:
        print("Azure Blob Storage Python quickstart sample")
        
        connect_str = "DefaultEndpointsProtocol=https;AccountName=assignement2functionapp;AccountKey=Mk7tRysj9/grCNyWA4jrz5CQ5bUebi+jwm/9+VYFpKNVa8GeZq1J1ly1pTNrsOVsdR5wJd6jW7Oq+AStLyP6eA==;EndpointSuffix=core.windows.net"

        # Create the BlobServiceClient object
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        container_client = blob_service_client.get_container_client("wordcountcontainer")
        content = []
        counter = 1
        for fileName in fileNames:
            blob_client = container_client.get_blob_client(fileName)
            for line in blob_client.download_blob().readall().decode('utf-8').split("\r\n"):
                content.append((counter, line))
                counter += 1

        return content
        

    except Exception as ex:
        print('Exception:')
        print(ex)
