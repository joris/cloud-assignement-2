# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt
# The reducer should take as input a key-value pair, where key is a word and value is a list (which will be all 1s). 
# The reducer should add up all the values in the list and produce as output a key—value pair where key is word and 
# value is total count. This is what we saw in lecture 4 – look at the slide deck for pseudocode. 

import logging

def main(pair: list) -> list:
    returnPair = [pair[0], 0]

    for _ in pair[1]:
        returnPair[1] += 1
    
    return returnPair
