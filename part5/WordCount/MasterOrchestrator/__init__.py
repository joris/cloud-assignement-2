# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df

def orchestrator_function(context: df.DurableOrchestrationContext):
    lines = yield context.call_activity("GetInputDataFn", ["mrinput-1.txt", "mrinput-2.txt", "mrinput-3.txt", "mrinput-4.txt"])

    mappers = [context.call_activity('Mapper', line) for line in lines]
    resultMapper = yield context.task_all(mappers)

    resultShuffler = yield context.call_activity('Shuffler', resultMapper)

    reducers = [context.call_activity('Reducer', keyValuePair) for keyValuePair in resultShuffler]
    resultReducer = yield context.task_all(reducers)

    return resultReducer

main = df.Orchestrator.create(orchestrator_function)