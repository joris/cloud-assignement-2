from numerical_integration import compute_integral
from flask import Flask

app = Flask(__name__)

@app.route('/')
def my_app():
    return 'Welcome to this numerical integration service. You can use the route /numericalintegralservice/minInterval/maxInterval to do your computation. Example: /numericalintegralservice/0/3.14159'

@app.route('/numericalintegralservice/<minInterval>/<maxInterval>')
def numerical_integral_service(minInterval, maxInterval):
    return 'The result is ' + str(compute_integral(1000000, float(minInterval), float(maxInterval)))
