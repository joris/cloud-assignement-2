import math

def abs_sin(x : float) -> float:
    """
    Given a input x, returns abs(sin(x)).

    Argument:
    ---------
    - `x`: An input float.

    Return:
    -------
    - The value of abs(sin(x)).
    """
    return abs(math.sin(x))

def compute_integral(N : int, minInterval : float,
                     maxInterval : float) -> float:
    """
    Given a N, a min and a max interval, returns the numerical integration of 
    (abs(sin(x))) in this interval.

    Argument:
    ---------
    - `N`: The number of subintervals to make the integration.
    - `minInterval`: The minimal bound for the integration.
    - `maxInterval`: The maximal bound for the integration.
    
    Return:
    -------
    - The result of the numerical integration of abs(sin(x)) between 
      minInterval and maxInterval.
    """
    result = 0.0;
    dx = (maxInterval - minInterval) / N

    for i in range(N):
        xip12 = dx * (i + 0.5)
        dI = abs_sin(xip12) * dx
        result += dI
    
    return result

if __name__ == "__main__":
    for i in range(1, 7):
        print("Value of N: {}, result of the numerical integration of "
              "abs(sin(x)) from 0 to pi: {}".format(10**i, 
              compute_integral(10**i, 0, 3.14159)))
