import re

pattern = '[0-9]\.[0-9][0.9]$'

file1 = open('locust_output_4.txt', 'r')
Lines = file1.readlines()

data = []
count = 0

for line in Lines:
    
    for i in line.split():
        if re.match(pattern, i) and (not (count % 4)):
            data.append(float(i))
            
        count += 1


file2 = open('locust_output_4_data.txt', 'w')

file2.write(str(data))
file2.close()

